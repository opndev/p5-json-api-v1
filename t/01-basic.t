use Test::Lib;
use Test::JSON::API::v1;

my $object = new_toplevel();

cmp_object_json(
    $object,
    {
        data => undef,
    },
    "Our JSON API object contains the minimal set of data"
);

$object = new_toplevel(data => new_resource());
cmp_object_json(
    $object,
    {
        data => undef,
    },
    ".. and an empty resource as data is also represented correctly",
);

my $resource = new_resource(
    id => 1,
    type => 'example',
);

$object = new_toplevel(data => $resource);
cmp_object_json(
    $object,
    {
        data => {
            id => 1,
            type => 'example',
        },
    },
    ".. and minimal resource as data is also represented correctly",
);

$resource = new_resource(
    id => 1,
    type => 'example',
    attributes => {
        title => 'bar',
    },
);

$object = new_toplevel(data => $resource);

cmp_object_json(
    $object,
    {
        data => {
            id   => 1,
            type => 'example',
            attributes => {
                title => 'bar'
            },
        },
    },
    ".. and now contains attributes"
);

$object = new_toplevel(
    data => $resource,
    errors => [ ]
);

cmp_object_json(
    $object,
    {
        errors => [
        ],
    },
    ".. we now return an error state",
);

done_testing;
