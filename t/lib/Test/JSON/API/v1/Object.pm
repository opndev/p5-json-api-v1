package Test::JSON::API::v1::Object;
use warnings;
use strict;

use Exporter qw(import);

our @EXPORT = qw(
    new_resource
    new_toplevel
    new_link
);

sub new_resource {
    require JSON::API::v1::Object::Resource;
    return JSON::API::v1::Object::Resource->new(@_);
}

sub new_toplevel {
    require JSON::API::v1;
    return JSON::API::v1->new(@_);
}

sub new_link {
    require JSON::API::v1::Object::Links;
    return JSON::API::v1::Object::Links->new(@_);
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

