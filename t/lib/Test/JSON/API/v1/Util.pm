package Test::JSON::API::v1::Util;
use warnings;
use strict;

use Exporter qw(import);
use Test::Deep qw(cmp_deeply);

our @EXPORT = qw(
    cmp_object_json
);

sub cmp_object_json {
    my ($object, $want, $msg) = @_;

   return cmp_deeply($object->TO_API_V1_JSON, $want, $msg);
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

