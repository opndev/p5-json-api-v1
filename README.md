# DESCRIPTION

This module attempts to make a Moose object behave like a JSON API object as
defined by [jsonapi.org](https://metacpan.org/pod/jsonapi.org). This object adheres to the v1 specification

# SYNOPSIS

# ATTRIBUTES

# METHODS
