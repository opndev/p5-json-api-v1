use utf8;

package JSON::API::v1;
our $VERSION = '0.001';
use Moose;
use namespace::autoclean;
use Carp qw(croak);


# ABSTRACT: A JSON API object according to jsonapi.org v1 specification

has data => (
    is => 'ro',
    isa => 'JSON::API::v1::Object::Resource',
    predicate => 'has_data',
);

has errors => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_errors',
);

has meta_object => (
    is        => 'ro',
    isa       => 'HashRef',
    predicate => 'has_meta_object',
);

has jsonapi => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_jsonapi',
);

has links => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_links',
);

has included => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_included',
);

sub as_data_object {
    my $self = shift;

    croak("You called me as a data object, but I'm in an error state!")
        if $self->has_errors;

    if ($self->has_data) {
        return $self->data->TO_API_V1_JSON;
    }
    return { data => undef };


}

sub as_error_object {
    my $self = shift;
    return { errors => $self->errors } if $self->has_errors;
    croak("You called me as an error object, but I'm not in an error state!");
}

sub TO_API_V1_JSON {
    my $self = shift;

    return $self->as_error_object if $self->has_errors;
    return $self->as_data_object;

}

sub as_relationship {
    my $self = shift;

    return {
        self  => $self->id,
        links => $self->type,
    };
}


with 'JSON::API::v1::Roles::TO_JSON';

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This module attempts to make a Moose object behave like a JSON API object as
defined by L<jsonapi.org>. This object adheres to the v1 specification

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 METHODS
