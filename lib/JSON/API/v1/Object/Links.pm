use utf8;

package JSON::API::v1::Object::Links;
our $VERSION = '0.001';
use Moose;
use namespace::autoclean;
use Carp qw(croak);
use MooseX::Types::URI qw(Uri);

# ABSTRACT: A JSON API Links object

has uri => (
    is        => 'ro',
    isa       => Uri,
    predicate => 'has_uri',
    coerce    => 1,
);

has meta_object => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_meta_object',
    init_arg  => 'meta',
);

has related => (
    is        => 'ro',
    isa       => 'JSON::API::v1::Object::Links',
    predicate => 'has_related',
);

sub TO_API_V1_JSON_NESTED {
    my $self = shift;

    if (!$self->has_uri && !$self->has_related) {
        croak(
                  "Unable to represent a link data object, both related"
                . "and uri are missing",
        );
    }
    if ($self->has_meta_object) {
        return {
            meta => $self->meta_object,
            href => $self->uri,
            $self->has_related
                ? (related => $self->related->TO_API_V1_JSON_NESTED)
                : (),
        };
    }
    return {
        $self->has_uri ? (self => $self->uri) : (),
        $self->has_related
            ? (related => $self->related->TO_API_V1_JSON_NESTED)
            : (),
    }
}

# Make sure we stringify the URI
around uri => sub {
    my ($orig, $self) = @_;
    return $self->$orig . "";
};

sub TO_API_V1_JSON {
    my $self = shift;
    my $rv = $self->TO_API_V1_JSON_NESTED;
    return { links => $rv };
}

with 'JSON::API::v1::Roles::TO_JSON';

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This module attempts to make a Moose object behave like a JSON API object as
defined by L<jsonapi.org>. This object adheres to the v1 specification.

=head1 SYNOPSIS

    use JSON::API::v1::Object::Resource;
    my $object = JSON::API::v1::Object::Resource->new(
        # If omitted, this becomes a "NULL" object
        id   => 1,
        type => 'example',

        # optional
        attributes => {
            'title' => 'Some example you are',
        },
    );

    $object->TO_JSON_API_V1;

=head1 ATTRIBUTES

=head1 METHODS

=head1 SEE ALSO

=over

=item * L<https://jsonapi.org/format/#document-resource-objects>

=back
